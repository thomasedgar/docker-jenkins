mkdir -p .logs
docker run \
  --name jenkins-tutorials \
  --rm \
  -u root \
  -p 8080:8080 \
  -v jenkins-data:/var/jenkins_home \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v "$HOME":/home \
  jenkinsci/blueocean > .logs/jenkins-tutorials.$(date '+%Y%m%d%H%M%S').log 2>&1 & 
